package org.geocrowd;

public class Constants {
	

	public static double alpha = 0.1;

	/** The Constant EXPERTISE_MATCH_SCORE. */
	public static final double EXPERTISE_MATCH_SCORE = 1.5;

	/** The Constant NON_EXPERTISE_MATCH_SCORE. */
	public static final double NON_EXPERTISE_MATCH_SCORE = 1;

	public static boolean useLocationEntropy = true;
}
